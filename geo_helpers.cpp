#include "geo_helpers.h"

#include <cmath>

constexpr double kEarthRadiusM = 6378137.0;

double deg_to_rad(double const & val)
{
    return val / (180.0 / M_PI);
}

double rad_to_deg(double const & val)
{
    return val * (180.0 / M_PI);
}

double get_dist(const GeoCoord & p1, const GeoCoord & p2)
{
    const double dLat = deg_to_rad(p2.lat - p1.lat);
    const double dLon = deg_to_rad(p2.lon - p1.lon);

    const double a = std::sin(dLat / 2.0) * std::sin(dLat / 2.0) +
                     std::cos(deg_to_rad(p1.lat)) * std::cos(deg_to_rad(p2.lat)) *
                         std::sin(dLon / 2.0) * std::sin(dLon / 2.0);

    const double c = 2.0 * std::atan2(std::sqrt(a), std::sqrt(1.0 - a));

    return kEarthRadiusM * c;
}

double get_dist_rate(const double & geo_dist)
{
    if (geo_dist < 20'000.0)
        return 10.0;

    if (geo_dist < 50'000.0)
        return 9.0;

    if (geo_dist < 70'000.0)
        return 8.0;

    if (geo_dist < 100'000.0)
        return 6.0;

    if (geo_dist < 150'000.0)
        return 4.0;

    if (geo_dist < 300'000.0)
        return 2.0;

    return 1.0;
}

double y_to_lat(const double & y)
{
    return rad_to_deg(2.0 * atan(exp(y / kEarthRadiusM)) - M_PI / 2.0);
}

double x_to_lon(const double & x)
{
    return rad_to_deg(x / kEarthRadiusM);
}

double lat_to_y(const double & lat)
{
    return log(tan(deg_to_rad(lat) / 2.0 + M_PI / 4.0)) * kEarthRadiusM;
}

double lon_to_x(const double & lon)
{
    return deg_to_rad(lon) * kEarthRadiusM;
}

CartesianCoord convert_to_cart(const GeoCoord & point)
{
    return CartesianCoord(lon_to_x(point.lon), lat_to_y(point.lat));
}

GeoCoord convert_to_geo(const CartesianCoord & point)
{
    return GeoCoord(y_to_lat(point.y), x_to_lon(point.x));
}