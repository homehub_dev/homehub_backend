# HomeHub geo suggester
## Description
Simplest service for searching on map. It finds house addresses, roads, cities, residential complexes, subway stations etc. 
It can be used as HTTP service or as console application for debugging.
In format of HTTP service it parses url arguments and returns answer in json format.
## Input
The input of the HTTP service is the set of url arguments:
```q``` - url-encoded search query. For example, "озерная".
```lat``` - latitude part of bounding box center of the map.
```lon``` - longitude part of bounding box center of the map.
```z``` - zoom level of the map.
```n``` - maximum count of search results to be returned.

Query example:
```
"localhost:8080/?q=%D0%BE%D0%B7%D0%B5%D1%80%D0%BD%D0%B0%D1%8F&lat=55.7597&lon=37.5414&z=9.7&n=2"
```

## Output
Service returns json with search results.

Example of answer:
```json
{
   "suggests":[
      {
         "city":"Москва",
         "geojson":{
            "coordinates":[
               [
                  37.448729654,
                  55.670505194
               ]
            ],
            "type":"MultiPoint"
         },
         "object_type":"subway_stations",
         "text":"Озёрная"
      },
      {
         "city":"Москва",
         "geojson":{
            "coordinates":[
               [
                  [
                     37.435721862,
                     55.664750347
                  ],
                  [
                     37.436194182,
                     55.665171453
                  ],
                  [
                     37.435721862,
                     55.664750347
                  ]
               ]
            ],
            "type":"Polygon"
         },
         "object_type":"building",
         "text":"Озёрная улица, 50"
      }
   ]
}
```

##Building and running
To build and run geo suggester you need docker to follow these steps:
```bash
cd images
sh run_docker.sh
```
```run_docker.sh``` script builds the docker image for service and starts container which listens on port 8080.

If you want to build and run geo suggester without docker you need to:
- Install dependencies listed in images/Dockerfile.
- Edit config file (images/geo_suggester.yml).
- Build ```geo_suggester_svc``` target and run it with single argument (path to config).