#pragma once

#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "geo_helpers.h"

using FeatureId = long long int;

struct ObjectOnMap
{
    std::string geojson;
    std::string object_type;
};

struct SuggestedObject
{
    std::string main_text;
    // TODO: fill this field with full address data (city, district, etc):
    std::string additional_text;
    std::string city;
    ObjectOnMap object_on_map;
};

struct UserOptions
{
    std::string query;
    size_t max_suggests = 10;
    double zoom = 9.0;
    GeoCoord coord;
};

struct AddressRecord
{
    std::string query;
    FeatureId city_id = 0;
    double place_rate = 0.0;
    size_t tokens_count = 0;
    ObjectOnMap object_on_map;
};

struct TokenData
{
    double idf = 0;
    std::unordered_set<size_t> docs;
};

using Tokens = std::unordered_map<std::string, TokenData>;
using TokensIt = Tokens::const_iterator;

struct Addresses
{
    void build_index();
    Tokens tokens;
    std::vector<AddressRecord> data;
    double max_idf = 0.0;
};

struct CityRecord
{
    std::string name;
    GeoCoord center;
    double rate = 0.0;
};

struct Cities
{
    std::unordered_map<FeatureId, CityRecord> data;
};

struct MatchedDoc
{
    double tokens_idf_sum = 0.0;
    size_t tokens_count = 0;
};

struct RatedDoc
{
    size_t doc_id = 0;
    double match_rate = 0.0;
    double tokens_idf_sum = 0.0;
    double city_rate = 0.0;
    double place_rate = 0.0;
    double geo_dist_rate = 0.0;
    double multi_rate = 0.0;
};

using RatedDocs = std::vector<RatedDoc>;

struct MatchedDocs
{
    void update_max_matched_tokens_count(size_t count);
    RatedDocs get_rated_docs() const;

    std::unordered_map<size_t, MatchedDoc> id_to_doc;
    size_t max_matched_tokens_count = 0;
};


class GeoSuggester
{
  public:
    // Loads necessary address data from files:
    bool load_data(std::string const & data_dir);
    // Finds geo suggests based on user query and other options:
    std::vector<SuggestedObject> suggest(const UserOptions & options) const;

  private:
    // Appends to addresses inverted index cities data:
    void add_cities_to_addresses();

    // Fills different rates (distance rate, place rate, etc) for documents:
    void calc_rate_for_docs(RatedDocs & rated_docs, size_t matched_tokens_count,
                            const UserOptions & options) const;


    // Prepares relevant documents for json'izing:
    std::vector<SuggestedObject> convert_docs_to_suggests(RatedDocs & rated_docs,
                                                          size_t max_suggests) const;

    Addresses addresses;
    Cities cities;
};


std::string to_json(std::vector<SuggestedObject> const & suggests);

[[nodiscard]] bool load_data(const std::string & data_path, GeoSuggester & suggester);