#pragma once

#include "spdlog/async.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/spdlog.h"

#include <memory>
#include <string>

class Log
{
  public:
    static void init(bool log_to_cout, bool log_to_file, std::string const & log_file);
    static void set_level(const std::string & level);

    template<typename FormatString, typename... Args>
    static void debug(const FormatString & fmt, Args &&... args)
    {
        log->debug(fmt, std::forward<Args>(args)...);
    }

    template<typename FormatString, typename... Args>
    static void info(const FormatString & fmt, Args &&... args)
    {
        log->info(fmt, std::forward<Args>(args)...);
    }

    template<typename FormatString, typename... Args>
    static void warn(const FormatString & fmt, Args &&... args)
    {
        log->warn(fmt, std::forward<Args>(args)...);
    }

    template<typename FormatString, typename... Args>
    static void error(const FormatString & fmt, Args &&... args)
    {
        log->error(fmt, std::forward<Args>(args)...);
    }

    template<typename FormatString, typename... Args>
    static void critical(const FormatString & fmt, Args &&... args)
    {
        log->critical(fmt, std::forward<Args>(args)...);
    }

  private:
    static std::shared_ptr<spdlog::logger> log;
};
