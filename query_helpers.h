#pragma once

#include "geo_helpers.h"

#include <string>
#include <vector>

std::string normalize(const std::string & s);

// For query tokenizing:
std::vector<std::string> tokenize(const std::string & str);

bool is_separator(char c);

bool has_digit(const std::string & s);

void replace(std::string & str, const std::string & substr);

void normalize_address(std::vector<std::string> & tokens);

// For uri tokenizing:
std::vector<std::string> tokenize(const std::string & str, const std::string & delims);

using Args = std::map<std::string, std::string>;

Args parse_args(const std::string & uri);