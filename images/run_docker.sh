#!/bin/bash

docker build -t antitail/geo_suggester:latest .
docker run -v /var/geo_suggester:/var/geo_suggester -v /var/log/geo_suggester:/var/log/geo_suggester -v /maps_services/backend:/build_dir/project -p 8080:8080  antitail/geo_suggester:latest
