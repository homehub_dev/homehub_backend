#!/bin/bash

wget https://boostorg.jfrog.io/artifactory/main/release/1.69.0/source/boost_1_69_0.tar.bz2
tar -xf boost_1_69_0.tar.bz2 
cd boost_1_69_0 || exit
./bootstrap.sh 
./b2 install  boost.locale.icu=on --prefix=/usr/local/boost169 
cd .. || exit
rm -rf boost_1_69_0
rm boost_1_69_0.tar.bz2
