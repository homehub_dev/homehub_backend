#/bin/bash

cd project
rm -rf build
rm CMakeCache.txt

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBOOST_INCLUDEDIR=/usr/local/boost169/include \
-DBOOST_LIBRARYDIR=/usr/local/boost169/lib -DBOOST_ROOT=/usr/local/boost169  .. 

make -j2
