#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"

#include "../query_helpers.h"
#include "../suggester.h"

#include <iostream>

TEST_SUITE_BEGIN("Parse & tokenize query");

TEST_CASE("Normalize 1")
{
    REQUIRE_EQ(normalize("ш."), "шоссе");
    REQUIRE_EQ(normalize("Пр-т"), "проспект");
}

TEST_CASE("Tokenize 1")
{
    const std::string query = "Взлётная улица, 5 к1";
    const auto & tokens = tokenize(query);

    REQUIRE_EQ(tokens.size(), 4);
    REQUIRE_EQ(tokens[0], "взлетная");
    REQUIRE_EQ(tokens[1], "улица");
    REQUIRE_EQ(tokens[2], "5");
    REQUIRE_EQ(tokens[3], "к1");
}

TEST_CASE("Tokenize 2")
{
    const std::string query = "Хорошёвская наб., дом 17А";
    const auto & tokens = tokenize(query);

    REQUIRE_EQ(tokens.size(), 3);
    REQUIRE_EQ(tokens[0], "хорошевская");
    REQUIRE_EQ(tokens[1], "набережная");
    REQUIRE_EQ(tokens[2], "17а");
}

TEST_CASE("Tokenize 3")
{
    const std::string query = "наты бабушкиной 5 корп 8";
    const auto & tokens = tokenize(query);

    REQUIRE_EQ(tokens.size(), 4);
    REQUIRE_EQ(tokens[0], "наты");
    REQUIRE_EQ(tokens[1], "бабушкиной");
    REQUIRE_EQ(tokens[2], "5");
    REQUIRE_EQ(tokens[3], "к8");
}

TEST_CASE("Tokenize 4")
{
    const std::vector<std::string> queries{
        "Улица Большая Якиманка, дом 14, корпус 2, строение 3.",
        "ул. большая Якиманка д 14 корп 2 стр 3",
        "ул. большая Якиманка д 14 к 2 с 3",
        "ул. большая Якиманка д14 корпус2 стр3",
        "ул большая Якиманка 14 к2 с3",
    };

    for (const auto & query : queries)
    {
        const auto & tokens = tokenize(query);

        REQUIRE_EQ(tokens.size(), 6);
        REQUIRE_EQ(tokens[0], "улица");
        REQUIRE_EQ(tokens[1], "большая");
        REQUIRE_EQ(tokens[2], "якиманка");
        REQUIRE_EQ(tokens[3], "14");
        REQUIRE_EQ(tokens[4], "к2");
        REQUIRE_EQ(tokens[5], "с3");
    }
}

TEST_CASE("Tokenize residential area 1")
{
    const std::string query = "Жилой комплекс «Семёновский парк»";
    const auto & tokens = tokenize(query);

    REQUIRE_EQ(tokens.size(), 3);
    REQUIRE_EQ(tokens[0], "жк");
    REQUIRE_EQ(tokens[1], "семеновский");
    REQUIRE_EQ(tokens[2], "парк");
}

TEST_SUITE_END();
