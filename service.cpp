#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include <boost/asio/thread_pool.hpp>

#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <optional>
#include <thread>

#include <curl/curl.h>
#include <yaml-cpp/yaml.h>

#include "log.h"
#include "suggester.h"
#include "query_helpers.h"

namespace be = boost::beast;
namespace asio = boost::asio;
using tcp = boost::asio::ip::tcp;

void check_field_presence(const YAML::Node & conf, const std::string & field)
{
    if (!conf[field])
        throw std::runtime_error("Error parsing yaml conf: absent field " + field);
}

YAML::Node load_conf(const std::string & path)
{
    YAML::Node conf = YAML::LoadFile(path);
    for (const auto & field : {"path_suggests", "threads", "port", "address", "log_path",
                               "log_level", "log_to_file", "log_to_cout"})
        check_field_presence(conf, field);

    return conf;
}

std::string unescape(const std::string & s)
{
    const auto unescaped_s = std::unique_ptr<char, decltype(&curl_free)>(
        curl_easy_unescape(nullptr, s.c_str(), s.size(), nullptr), &curl_free);
    return std::string(unescaped_s.get());
}

void log_error(be::error_code error, std::string && operation)
{
    Log::error("Error in session, operation '{}': {}", operation, error.message());
}

[[nodiscard]] bool get_double(const std::string & s, double & val)
{
    try
    {
        val = std::stod(s);
        return true;
    }
    catch (const std::invalid_argument & e)
    {
        Log::error("Invalid value in line: {}", s);
    }
    catch (const std::out_of_range & e)
    {
        Log::error("Out of range value in line: {}", s);
    }
    return false;
}

std::optional<UserOptions>
get_user_options(const be::http::request<be::http::string_body> & request)
{
    const std::string params_part_of_uri{request.target()};
    Log::info("URI: {}", params_part_of_uri);

    try
    {
        const Args & args = parse_args(params_part_of_uri);

        auto it_query = args.find("q");
        if (it_query == args.end())
            return std::nullopt;

        UserOptions opts;
        opts.query = unescape(it_query->second);

        auto it_lat = args.find("lat");
        if (it_lat == args.end())
            return std::nullopt;

        if (!get_double(it_lat->second, opts.coord.lat))
            return std::nullopt;

        auto it_lon = args.find("lon");
        if (it_lon == args.end())
            return std::nullopt;

        if (!get_double(it_lon->second, opts.coord.lon))
            return std::nullopt;

        auto it_zoom = args.find("z");
        if (it_zoom == args.end())
            return std::nullopt;

        if (!get_double(it_zoom->second, opts.zoom))
            return std::nullopt;

        if (opts.zoom < 0.0 || opts.zoom > 22.0)
            return std::nullopt;

        auto it_count = args.find("n");
        if (it_count != args.end())
        {
            double count = 0.0;
            if (!get_double(it_count->second, count))
                return std::nullopt;

            if (count >= 1 && count <= 15)
                opts.max_suggests = static_cast<size_t>(count);
        }

        return opts;
    }
    catch(const std::exception & e)
    {
        Log::critical("Exception {} parsing uri: {}", e.what(), params_part_of_uri);
        return std::nullopt;
    }
}

class session_handler {
  public:
    session_handler(asio::ip::tcp::socket && socket, GeoSuggester & suggester):
    socket_(std::move(socket)), suggester_(suggester) {}

    void operator()() {
        be::error_code error;

        while (true)
        {
            be::flat_buffer buffer;

            be::http::request<be::http::string_body> request;
            be::http::read(socket_, buffer, request, error);

            if (error == be::http::error::end_of_stream)
                break;

            if (error)
            {
                log_error(error, "read");
                return;
            }

            be::http::response<be::http::string_body> response{be::http::status::ok, request.version()};

            response.set(be::http::field::content_type, "application/json; charset=utf-8");
            response.set(be::http::field::access_control_allow_origin, "*");

            response.keep_alive(request.keep_alive());

            const std::optional<UserOptions> & opts = get_user_options(request);

            if (opts.has_value())
            {
                response.body() = to_json(suggester_.suggest(opts.value()));
            }
            else
            {
                static const std::string empty_json = "{}";
                response.body() = empty_json;
            }

            response.prepare_payload();

            be::http::write(socket_, response, error);

            if (error)
            {
                log_error(error, "write");
                return;
            }
        }

        socket_.shutdown(tcp::socket::shutdown_send, error);
    }

  private:
    asio::ip::tcp::socket socket_;
    GeoSuggester & suggester_;
};

int main(int argc, char * argv[])
{
    if (argc == 1)
        throw std::runtime_error("Pass path to config as arg");

    const std::string conf_path = std::string(argv[1]);
    const YAML::Node & conf = load_conf(conf_path);

    Log::init(conf["log_to_cout"].as<bool>(), conf["log_to_file"].as<bool>(),
              conf["log_path"].as<std::string>());
    Log::set_level(conf["log_level"].as<std::string>());

    GeoSuggester suggester;
    if (!load_data(conf["path_suggests"].as<std::string>(), suggester))
        return EXIT_FAILURE;

    auto const thread_count = conf["threads"].as<int>();
    boost::asio::thread_pool suggester_pool(thread_count);

    try
    {
        const auto address = asio::ip::make_address(conf["address"].as<std::string>());
        const auto port = conf["port"].as<uint16_t>();

        Log::info("Started listening on {}:{}", address.to_string(), port);

        asio::io_context context{thread_count};
        tcp::acceptor acceptor{context, {address, port}};

        while (true)
        {
            asio::ip::tcp::socket socket = acceptor.accept(context);
            if (!socket.is_open())
                break;

            try
            {
                boost::asio::post(suggester_pool,
                                  session_handler(std::move(socket), std::ref(suggester)));
            }
            catch(std::exception const & ex_req)
            {
                Log::error("Exception while handling request: {}", ex_req.what());
            }
        }
    }
    catch (std::exception const & ex)
    {
        Log::critical("Exception in main loop: {}", ex.what());
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}