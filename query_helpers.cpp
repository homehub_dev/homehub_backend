#include <map>

#include <unicode/locid.h>
#include <unicode/schriter.h>
#include <unicode/uchar.h>
#include <unicode/ucnv.h>
#include <unicode/unistr.h>

#include "query_helpers.h"

namespace
{
    using icu_string = icu::UnicodeString;


    std::string get_str(const icu_string & str);
    icu_string get_icu_str(const std::string & str);

    std::string get_str(const icu_string & str)
    {
        std::string res;
        str.toUTF8String(res);
        return res;
    }

    icu_string get_icu_str(const std::string & str)
    {
        const std::size_t len = str.size();
        std::vector<UChar> target(len);

        UErrorCode status = U_ZERO_ERROR;
        UConverter * conv = ucnv_open("utf-8", &status);
        if (!U_SUCCESS(status))
            return icu_string();

        int32_t target_len = ucnv_toUChars(conv, target.data(), len, str.c_str(), len, &status);
        if (!U_SUCCESS(status))
            return icu_string();
        ucnv_close(conv);
        return icu_string(target.data(), target_len);
    }
}// namespace

void replace(std::string & str, const std::string & substr, const std::string & new_substr)
{
    size_t index = 0;
    const size_t len = new_substr.size();

    while (true)
    {
        index = str.find(substr, index);
        if (index == std::string::npos)
            break;

        str.replace(index, len, new_substr);

        index += len;
    }
}

std::string normalize(const std::string & s)
{
    icu_string wstr = get_icu_str(s);
    wstr = wstr.toLower("ru_RU");
    std::string norm = get_str(wstr);

    if (norm.size() > 1 && norm.back() == '.')
        norm.pop_back();

    replace(norm, "ё", "е");

    static const std::map<std::string, std::string> replacements{
        {"ул", "улица"},    {"п-т", "проспект"},   {"пр-т", "проспект"},  {"пр-кт", "проспект"},
        {"пр", "проезд"},   {"пр-д", "проезд"},    {"пл", "площадь"},     {"ш", "шоссе"},
        {"б-р", "бульвар"}, {"бульв", "бульвар"},  {"кв", "квартал"},     {"кв-л", "квартал"},
        {"корп", "к"},      {"корпус", "к"},       {"дом", ""},           {"д", ""},
        {"ал", "аллея"},    {"мкр", "микрорайон"}, {"наб", "набережная"}, {"пер", "переулок"},
        {"стр", "с"},       {"строение", "с"}};

    static const std::vector<std::pair<std::string, std::string>> bld_replacements{
        {"корпус", "к"}, {"корп", "к"}, {"строение", "с"}, {"стр", "с"}, {"дом", ""}, {"д", ""}};

    auto it = replacements.find(norm);
    if (it != replacements.end())
    {
        norm = it->second;
    }
    else
    {
        if (has_digit(norm))
        {
            for (const auto & [src, target] : bld_replacements)
            {
                if (norm.find(src) == 0)
                    return target + norm.substr(src.size());
            }
        }
    }

    return norm;
}

bool is_separator(char c)
{
    static const std::string separators = " ,.;\t\n\"-()";
    return separators.find(c) != std::string::npos;
}

bool has_digit(const std::string & s)
{
    return s.find_first_of("0123456789") != std::string::npos;
}

void normalize_address(std::vector<std::string> & tokens)
{
    if (tokens.size() < 2)
        return;

    std::vector<size_t> idx_for_removal;

    size_t i = 0;

    while (i < tokens.size() - 1)
    {
        const std::string cur = tokens[i];
        const bool is_bld_part = cur == "к" || cur == "с";
        const bool is_residential_part = cur == "жилой";

        bool replaced = false;

        if (is_bld_part || is_residential_part)
        {
            std::string & next = tokens[i + 1];

            if (is_bld_part && has_digit(next))
            {
                replaced = true;
                next = cur + next;
            }
            else if (is_residential_part && next == "комплекс")
            {
                replaced = true;
                next = "жк";
            }

            if (replaced)
            {
                idx_for_removal.push_back(i);
                i += 2;
                continue;
            }
        }
        ++i;
    }

    for (auto it = idx_for_removal.rbegin(); it != idx_for_removal.rend(); ++it)
        tokens.erase(tokens.begin() + *it);
}

std::vector<std::string> tokenize(const std::string & in_str)
{
    std::string str = in_str;

    static const std::vector<std::string> symbols_to_replace{"«", "»", "—"};

    for (const auto & symbol_to_replace : symbols_to_replace)
        replace(str, symbol_to_replace, " ");

    size_t start_index = 0;

    std::vector<std::string> tokens;
    tokens.reserve(10);

    std::string token;
    token.reserve(str.size());

    for (size_t i = start_index; i < str.size(); ++i)
    {
        if (is_separator(str[i]))
        {
            token = normalize(token);

            if (!token.empty())
            {
                tokens.emplace_back(token);
                token.clear();
            }

            continue;
        }

        token += str[i];
    }

    token = normalize(token);
    if (!token.empty())
        tokens.emplace_back(token);

    normalize_address(tokens);
    return tokens;
}

std::vector<std::string> tokenize(const std::string & str, const std::string & delims)
{
    std::vector<std::string> tokens;
    size_t pos;
    size_t last_pos = 0;

    while(last_pos < str.size() + 1)
    {
        pos = str.find_first_of(delims, last_pos);
        if(pos == std::string::npos)
            pos = str.size();

        if(pos != last_pos)
            tokens.emplace_back(str.substr(last_pos, pos - last_pos));

        last_pos = pos + 1;
    }

    return tokens;
}

Args parse_args(const std::string & uri)
{
    constexpr size_t max_uri_len = 1000;
    constexpr size_t max_param_count = 10;

    if (uri.size() > max_uri_len)
        return {};

    size_t params_start_i = uri.find('?');
    if (params_start_i == std::string::npos || params_start_i == uri.size() - 1)
        return {};

    const std::vector<std::string> args_with_vals = tokenize(uri.substr(params_start_i + 1), "&");

    Args args;

    for (const auto & kv_str: args_with_vals)
    {
        const std::vector<std::string> kv = tokenize(kv_str, "=");

        if (kv.size() == 1)
            args[kv.front()] = "";

        else if (kv.size() == 2)
            args[kv.front()] = kv.back();

        // Suspicious uri, we skip the rest of the args:
        if (args.size() > max_param_count)
            break;
    }

    return args;
}