// Console mode is used for debugging purposes, not for production mode.
// For running as a service use service.cpp.

#include "log.h"
#include "suggester.h"

#include <chrono>
#include <iostream>

int main(int argc, char * argv[])
{
    Log::init(true, false, "");
    Log::set_level("debug");
    Log::info("Started suggester");

    if (argc < 2)
    {
        Log::error("Absent argument with path to data");
        return EXIT_FAILURE;
    }

    GeoSuggester suggester;

    if (!load_data(argv[1], suggester))
        return EXIT_FAILURE;

    UserOptions opts;
    opts.coord = GeoCoord(55.776, 37.628);

    Log::info("User input of queries:");

    for (std::string line; std::getline(std::cin, line);)
    {
        Log::info("User input : [{}]", line);
        opts.query = line;

        const auto ts = std::chrono::high_resolution_clock::now();

        const auto res = suggester.suggest(opts);

        const auto tf = std::chrono::high_resolution_clock::now();
        const auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(tf - ts).count();
        Log::info("Done in {} ms", delta);
    }

    Log::info("Stopped service.");
    return EXIT_SUCCESS;
}
