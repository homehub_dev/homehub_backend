#include <stdexcept>

#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include "log.h"

std::shared_ptr<spdlog::logger> Log::log;

void Log::init(bool log_to_cout, bool log_to_file, const std::string & log_file)
{
    spdlog::init_thread_pool(8192, 1);
    std::vector<spdlog::sink_ptr> sinks;

    if (log_to_cout)
    {
        auto stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        sinks.push_back(stdout_sink);
    }

    if (log_to_file)
    {
        auto rotating_sink =
            std::make_shared<spdlog::sinks::rotating_file_sink_mt>(log_file, 1024 * 1024 * 10, 3);
        sinks.push_back(rotating_sink);
    }

    if (sinks.empty())
        throw std::runtime_error("Specify at least one sink for logger");

    log = std::make_shared<spdlog::async_logger>("async_logger", sinks.begin(), sinks.end(),
                                                 spdlog::thread_pool(),
                                                 spdlog::async_overflow_policy::block);
    spdlog::register_logger(log);
}

void Log::set_level(std::string const & level)
{
    if (level == "critical")
    {
        log->set_level(spdlog::level::critical);
    }
    else if (level == "error")
    {
        log->set_level(spdlog::level::err);
    }
    else if (level == "warning")
    {
        log->set_level(spdlog::level::warn);
    }
    else if (level == "info")
    {
        log->set_level(spdlog::level::info);
    }
    else if (level == "debug")
    {
        log->set_level(spdlog::level::debug);
    }
    else
    {
        throw std::runtime_error("Error setting log level " + level);
    }
}
