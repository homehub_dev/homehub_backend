#include <cassert>
#include <cmath>
#include <fstream>
#include <map>
#include <optional>

#include "log.h"

#include "query_helpers.h"
#include "suggester.h"

#include "nlohmann/json.hpp"

namespace consts
{
    constexpr double min_idf = 1.0;
    constexpr FeatureId absent_city_id = 0;
    constexpr double absent_city_rate = 3.0;
    constexpr double absent_city_dist_rate = 4.0;
}// namespace consts

namespace
{
    struct CityData
    {
        double dist_to_city = 0.0;
        double city_rate = 0.0;
    };

    template<class T>
    [[nodiscard]] bool get_int(const std::string & s, T & val)
    {
        try
        {
            val = static_cast<T>(std::stoll(s));
            return true;
        }
        catch (const std::invalid_argument & e)
        {
            Log::error("Invalid value in line: {}", s);
        }
        catch (const std::out_of_range & e)
        {
            Log::error("Out of range value in line: {}", s);
        }
        return false;
    }

    double get_k_geo(double zoom)
    {
        if (zoom < 5.0)
            return 0.0;
        if (zoom < 8.0)
            return 0.2;
        if (zoom < 10.0)
            return 0.5;
        return 0.8;
    }

    // Searches for query tokens in inverted index:
    std::vector<TokensIt> match_tokens(const Tokens & all_tokens,
                                       const std::vector<std::string> & query_tokens)
    {
        std::vector<TokensIt> found_tokens;

        for (const auto & token : query_tokens)
        {
            if (const auto it = all_tokens.find(token); it != all_tokens.end())
                found_tokens.emplace_back(it);
        }

        std::sort(found_tokens.begin(), found_tokens.end(),
                  [](TokensIt l, TokensIt r) { return l->second.idf > r->second.idf; });

        Log::info("Sorted {} found tokens:", found_tokens.size());

        for (auto it : found_tokens)
        {
            Log::info("Found token [{}]\t idf {} \t count of docs {}", it->first, it->second.idf,
                      it->second.docs.size());
        }

        return found_tokens;
    }

    void update_doc_rate(const TokenData & token_data, MatchedDocs & matched_docs)
    {
        for (auto & [doc_id, data] : matched_docs.id_to_doc)
        {
            if (token_data.docs.count(doc_id) == 0)
                continue;

            data.tokens_idf_sum += token_data.idf;
            ++data.tokens_count;
            matched_docs.update_max_matched_tokens_count(data.tokens_count);
        }
    }

    MatchedDocs get_matched_docs(const std::vector<TokensIt> & found_tokens)
    {
        MatchedDocs matched_docs;
        // TODO: tune count of docs to be reserved:
        matched_docs.id_to_doc.reserve(1000);

        for (const auto & it_token : found_tokens)
        {
            const auto & idf = it_token->second.idf;
            const auto & docs = it_token->second.docs;

            // Token is too frequent so we don't add new docs, only update params of
            // already present docs:
            if (idf <= consts::min_idf)
            {
                update_doc_rate(it_token->second, matched_docs);
                continue;
            }

            for (const auto doc_id : docs)
            {
                auto & matched_doc = matched_docs.id_to_doc[doc_id];
                matched_doc.tokens_idf_sum += idf;
                ++matched_doc.tokens_count;

                matched_docs.update_max_matched_tokens_count(matched_doc.tokens_count);
            }
        }

        return matched_docs;
    }

    // Sorts documents by their relevance to user options:
    void sort_docs(RatedDocs & rated_docs, const UserOptions & options)
    {
        const double k_dist = get_k_geo(options.zoom);
        const double k_city = 1.0 - k_dist;
        constexpr double k_geo = 0.4;
        // Scale (0; 1] to (0; 10] with importance coefficient 0.4:
        constexpr double k_match = 0.4 * 10;
        constexpr double k_place = 0.2;

        for (auto & rated_doc : rated_docs)
        {
            const double geo_rate = rated_doc.geo_dist_rate * k_dist + rated_doc.city_rate * k_city;
            rated_doc.multi_rate =
                geo_rate * k_geo + rated_doc.match_rate * k_match + rated_doc.place_rate * k_place;
        }

        std::sort(rated_docs.begin(), rated_docs.end(), [](RatedDoc & lhs, RatedDoc & rhs) {
            if (lhs.multi_rate == rhs.multi_rate)
                return lhs.tokens_idf_sum > rhs.tokens_idf_sum;

            return lhs.multi_rate > rhs.multi_rate;
        });
    }

    template<class S>
    [[nodiscard]] bool read_coord(S & stream, GeoCoord & coord)
    {
        std::string line;

        try
        {
            if (!std::getline(stream, line) || line.empty())
                return false;

            coord.lon = std::stod(line);

            if (!std::getline(stream, line) || line.empty())
                return false;

            coord.lat = std::stod(line);

            return true;
        }
        catch (const std::invalid_argument & e)
        {
            Log::error("Invalid value in line: {}", line);
        }
        catch (const std::out_of_range & e)
        {
            Log::error("Out of range value in line: {}", line);
        }
        return false;
    }

    template<class S>
    std::optional<ObjectOnMap> read_object_on_map(S & stream)
    {
        std::string line;

        if (!std::getline(stream, line))
            return std::nullopt;

        ObjectOnMap obj;
        obj.geojson = line;

        if (!std::getline(stream, line))
            return std::nullopt;

        obj.object_type = line;

        return obj;
    }

    template<class S>
    [[nodiscard]] bool read_object_on_map(S & stream, ObjectOnMap & object_on_map)
    {
        std::optional<ObjectOnMap> obj = read_object_on_map(stream);
        if (!obj.has_value())
            return false;

        object_on_map = obj.value();

        return true;
    }

    template<class S, class C>
    [[nodiscard]] bool read_record(S & stream, C & container)
    {
        AddressRecord rec;

        if (!read_object_on_map(stream, rec.object_on_map))
            return false;

        std::string line;

        if (!std::getline(stream, line))
            return false;

        if (!get_int(line, rec.city_id))
            return false;

        if (!std::getline(stream, line))
            return false;

        if (!get_int(line, rec.place_rate))
            return false;

        if (!std::getline(stream, line))
            return false;

        rec.query = line;
        container.push_back(rec);
        return true;
    }

    template<class T>
    [[nodiscard]] bool read_record(T & stream,
                                   std::unordered_map<FeatureId, CityRecord> & container)
    {
        CityRecord rec;
        std::string line;

        if (!std::getline(stream, line))
            return false;

        FeatureId id;

        if (!get_int(line, id))
            return false;

        if (!read_coord(stream, rec.center))
            return false;

        if (!std::getline(stream, line))
            return false;

        if (!get_int(line, rec.rate))
            return false;

        if (!std::getline(stream, line))
            return false;

        rec.name = line;

        container.emplace(id, rec);
        return true;
    }


    template<class T>
    [[nodiscard]] bool read_file(const std::string & path, T & container)
    {
        Log::info("Started reading data from {}", path);
        std::ifstream file(path);

        if (!file.is_open())
        {
            Log::error("File could not be opened: {}", path);
            return false;
        }

        std::string line;
        std::getline(file, line);

        size_t count;
        if (!get_int(line, count))
            return false;

        container.reserve(count);
        Log::info("Reserved {} items in container for {}", count, path);

        while (true)
        {
            if (!read_record(file, container))
                break;
        }

        Log::info("Finished reading {} objects from {}", container.size(), path);
        return true;
    }
}// namespace

void MatchedDocs::update_max_matched_tokens_count(size_t count)
{
    if (count > max_matched_tokens_count)
        max_matched_tokens_count = count;
}

RatedDocs MatchedDocs::get_rated_docs() const
{
    RatedDocs rated_docs;
    // TODO: tune count of docs to be reserved:
    rated_docs.reserve(id_to_doc.size() / 4);

    for (auto & [doc_id, doc_data] : id_to_doc)
    {
        if (doc_data.tokens_count < max_matched_tokens_count)
            continue;

        RatedDoc doc;
        doc.doc_id = doc_id;
        doc.tokens_idf_sum = doc_data.tokens_idf_sum;

        rated_docs.push_back(doc);
    }

    return rated_docs;
}

void GeoSuggester::add_cities_to_addresses()
{
    for (auto [city_id, city_data] : cities.data)
    {
        AddressRecord rec;

        rec.query = city_data.name;
        rec.city_id = city_id;
        rec.place_rate = city_data.rate;
        rec.object_on_map.object_type = "towns";
        rec.object_on_map.geojson = "{\"type\":\"Point\",\"coordinates\": [" + 
        std::to_string(city_data.center.lon) +", " + std::to_string(city_data.center.lat) + "]}";

        addresses.data.push_back(rec);
    }
}

std::vector<SuggestedObject> GeoSuggester::convert_docs_to_suggests(RatedDocs & rated_docs,
                                                                    size_t max_suggests) const
{
    std::vector<SuggestedObject> res;
    res.reserve(max_suggests);

    size_t i = 0;
    for (auto rated_doc : rated_docs)
    {
        const auto & doc_id = rated_doc.doc_id;
        const auto & doc = addresses.data[doc_id];
        SuggestedObject obj;
        obj.main_text = doc.query;

        if (doc.city_id != consts::absent_city_id)
        {
            // TODO: fix this on the data pipeline side:
            auto it_city = cities.data.find(doc.city_id);
            if (it_city != cities.data.end())
                obj.city = it_city->second.name;
        }

        obj.object_on_map = doc.object_on_map;

        Log::info("Idx: {} | Rate: {} | Idf sum: {} | Match rate: {} | main text: {} | city: {}", i,
                  rated_doc.multi_rate, rated_doc.tokens_idf_sum, rated_doc.match_rate,
                  obj.main_text, obj.city);
        ++i;

        res.emplace_back(obj);
        if (res.size() >= max_suggests)
            return res;
    }

    return res;
}

void GeoSuggester::calc_rate_for_docs(RatedDocs & rated_docs, size_t matched_tokens_count,
                                      const UserOptions & options) const
{
    std::unordered_map<FeatureId, CityData> city_id_to_data;

    for (auto & rated_doc : rated_docs)
    {
        const auto & doc = addresses.data[rated_doc.doc_id];
        rated_doc.match_rate = matched_tokens_count / double(doc.tokens_count);

        auto [it_city_data, inserted] = city_id_to_data.emplace(doc.city_id, CityData{});
        CityData & city_data = it_city_data->second;

        if (inserted)
        {
            if (auto it_city = cities.data.find(doc.city_id); it_city != cities.data.end())
            {
                city_data.city_rate = it_city->second.rate;
                city_data.dist_to_city =
                    get_dist_rate(get_dist(it_city->second.center, options.coord));
            }
            else
            {
                city_data.city_rate = consts::absent_city_rate;
                city_data.dist_to_city = consts::absent_city_dist_rate;
            }
        }

        rated_doc.geo_dist_rate = it_city_data->second.dist_to_city;
        rated_doc.city_rate = it_city_data->second.city_rate;
        rated_doc.place_rate = doc.place_rate;
    }
}

std::vector<SuggestedObject> GeoSuggester::suggest(const UserOptions & options) const
{
    const std::vector<std::string> & query_tokens = tokenize(options.query);
    Log::info("Split query {} to {} tokens:", options.query, query_tokens.size());

    for (auto & t : query_tokens)
        Log::info("\t{}", t);

    // TODO handle "suggester" mode.

    const std::vector<TokensIt> & tokens = match_tokens(addresses.tokens, query_tokens);

    const MatchedDocs & matched_docs = get_matched_docs(tokens);

    RatedDocs rated_docs = matched_docs.get_rated_docs();

    calc_rate_for_docs(rated_docs, tokens.size(), options);

    sort_docs(rated_docs, options);

    return convert_docs_to_suggests(rated_docs, options.max_suggests);
}

bool GeoSuggester::load_data(std::string const & data_dir)
{
    if (!read_file(data_dir + "towns.txt", cities.data))
        return false;

    if (!read_file(data_dir + "buildings.txt", addresses.data))
        return false;

    if (!read_file(data_dir + "residential.txt", addresses.data))
        return false;

    if (!read_file(data_dir + "roads.txt", addresses.data))
        return false;

    if (!read_file(data_dir + "subway_stations.txt", addresses.data))
        return false;

    add_cities_to_addresses();
    addresses.build_index();

    return true;
}

void Addresses::build_index()
{
    Log::info("Started building index");

    for (size_t i = 0; i < data.size(); ++i)
    {
        auto & rec = data[i];
        const auto & words = tokenize(rec.query);

        for (const auto & word : words)
        {
            if (word.empty())
                continue;

            const auto & [it, _] = tokens.emplace(word, TokenData{});
            it->second.docs.emplace(i);
            ++rec.tokens_count;
        }
    }

    const size_t docs_count = data.size();
    Log::info("{} tokens in {} docs", tokens.size(), docs_count);

    for (auto & [token, tok_data] : tokens)
    {
        const size_t doc_freq = tok_data.docs.size();

        if (docs_count < doc_freq)
            Log::warn("Docs_count = {} < docs_freq = {}. Token {}", docs_count, doc_freq, token);

        tok_data.idf = std::max(0.1, std::log(docs_count / doc_freq));

        if (tok_data.idf > max_idf)
            max_idf = tok_data.idf;
    }

    Log::info("Max idf = {}", max_idf);
    Log::info("Finished building index");
}

std::string to_json(std::vector<SuggestedObject> const & suggests)
{
    nlohmann::json res;
    res["suggests"] = nlohmann::json::array();

    for (const auto & obj : suggests)
    {
        nlohmann::json obj_json;
        obj_json["text"] = obj.main_text;
        if (!obj.city.empty())
            obj_json["city"] = obj.city;

        obj_json["geojson"] = nlohmann::json::parse(obj.object_on_map.geojson);
        obj_json["object_type"] = obj.object_on_map.object_type;
        res["suggests"].push_back(obj_json);
    }

    return res.dump(2);
}

bool load_data(const std::string & data_path, GeoSuggester & suggester)
{
    const auto ts_load = std::chrono::high_resolution_clock::now();

    if (!suggester.load_data(data_path))
    {
        Log::error("Could not load data from {}", data_path);
        return false;
    }

    const auto tf_load = std::chrono::high_resolution_clock::now();
    const auto delta_load =
        std::chrono::duration_cast<std::chrono::milliseconds>(tf_load - ts_load).count();

    Log::info("Loaded data in {} ms", delta_load);
    return true;
}
