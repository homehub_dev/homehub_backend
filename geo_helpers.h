#pragma once

struct GeoCoord
{
    GeoCoord() = default;
    GeoCoord(double latitude, double longitude) : lat(latitude), lon(longitude)
    {
    }

    double lat = 0.0;
    double lon = 0.0;
};


struct CartesianCoord
{
    CartesianCoord() = default;
    CartesianCoord(double x_val, double y_val) : x(x_val), y(y_val)
    {
    }

    double x = 0.0;
    double y = 0.0;
};


// Implementation of the haversine formula which determines the great-circle distance
// between two points on a sphere given their longitudes and latitudes.
// https://en.wikipedia.org/wiki/Haversine_formula
double get_dist(const GeoCoord & p1, const GeoCoord & p2);

double get_dist_rate(const double & geo_dist);

CartesianCoord convert_to_cart(GeoCoord const & point);
GeoCoord convert_to_geo(CartesianCoord const & point);